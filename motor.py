#import GPIO
import RPi.GPIO as GPIO
from time import sleep

#set GPIO mode
GPIO.setmode(GPIO.BCM)

#setting up motor controlling pins
GPIO.setup(17,GPIO.OUT) #pin A
GPIO.setup(27,GPIO.OUT) #pin B
GPIO.setup(19,GPIO.OUT) #pin C
GPIO.setup(26,GPIO.OUT) #pin D

try:
    while True:
    	GPIO.output(17,True)
    	GPIO.output(27,False)
    	sleep(0.5)
    	GPIO.output(17,False)
    	GPIO.output(27,True)
    	sleep(0.5)
    	GPIO.output(19,True)
    	GPIO.output(26,False)
    	sleep(0.5)
    	GPIO.output(19,False)
    	GPIO.output(26,True)
    	sleep(0.5)
    	GPIO.output(17,False)
    	GPIO.output(27,False)
    	GPIO.output(19,False)
    	GPIO.output(26,False)
    	sleep(1.5)

finally:
    GPIO.cleanup()
#import GPIO
import RPi.GPIO as GPIO
from time import sleep

#set GPIO mode
GPIO.setmode(GPIO.BCM)

#setting up motor controlling pins
GPIO.setup(17,GPIO.OUT) #pin A
GPIO.setup(27,GPIO.OUT) #pin B
GPIO.setup(19,GPIO.OUT) #pin C
GPIO.setup(26,GPIO.OUT) #pin D

#Sensor inputs
GPIO.setup(21,GPIO.IN) #left sensor
GPIO.setup(20,GPIO.IN) #center sensor
GPIO.setup(16,GPIO.IN) #right sensor


def forward():
    GPIO.output(17,True)
    GPIO.output(27,False)
    GPIO.output(19,False)
    GPIO.output(26,True)

def stop():
    GPIO.output(17,False)
    GPIO.output(27,False)
    GPIO.output(19,False)
    GPIO.output(26,False)

def right():
    GPIO.output(17,False)
    GPIO.output(27,True)
    GPIO.output(19,False)
    GPIO.output(26,True)

def left():
    GPIO.output(17,True)
    GPIO.output(27,False)
    GPIO.output(19,True)
    GPIO.output(26,False)


try:
    while True:
      led_l = GPIO.input(21)
      led_c = GPIO.input(20)
      led_r = GPIO.input(16)

      if (led_l == True and led_c == True and led_r == True ):
          forward()
          sleep(0.01)
          stop()
      elif (led_l == True and led_c == False and led_r == True):
          forward()
      elif (led_l == False and led_c == False and led_r == True):
          left()
      elif (led_l == True and led_c == False and led_r == False):
          right()
      elif (led_l == False and led_c == True and led_r == True):
          left()
      elif (led_l == True and led_c == True and led_r == False):
          right()

finally:
    GPIO.cleanup()